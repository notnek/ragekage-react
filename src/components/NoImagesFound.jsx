import React from 'react';

const NoImagesFound = () => (
  <p>No images were found!</p>
);

export default NoImagesFound;
